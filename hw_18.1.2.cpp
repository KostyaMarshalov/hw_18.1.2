﻿
#include <iostream>
using namespace std;


class Stack
{
    private:
        int size;
        int index = 0;
        int *arr;
    public:
        Stack(int new_size)
        {
            size = new_size;
            arr = new int[size];
           
        }

        void GetArray()
        {
            for (index = 0; index < size; index++)
            {
                arr[index] = rand() % 20;
            }
        }

        void ShowArray()
        {
            for (index = 0; index < size; index++)
            {
                cout << arr[index] << "\t";
            }
            cout << endl;
        }

        void push_stack(int value)
        {
            int* newArray = new int[size + 1];
            for (index = 0; index < size; index++)
            {
                newArray[index] = arr[index];
            }
            newArray[size] = value;
            size++;
            delete[] arr;
            arr = newArray;
        }

        void pop_stack()
        {
            
            int* newArray = new int[size];
            if (size >= 1)
            {
                size--;

                for (index = 0; index < size; index++)
                {
                    newArray[index] = arr[index];
                }


                delete[] arr;
                arr = newArray;
            }
        }

        ~Stack()
        {
            delete[] arr;
        }
};

int main()
{

    Stack stack(5);
    stack.GetArray();
    stack.ShowArray();
    stack.push_stack(111);
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    stack.pop_stack();
    stack.ShowArray();
    return 0;
}